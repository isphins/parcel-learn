# Parcel Vue

```sh
$ npm install -g parcel-bundler 
$ npm install

# run
$ parcel index.html

# build
$ npx parcel build
```

ref : [by alligator](https://alligator.io/vuejs/vue-parceljs/)